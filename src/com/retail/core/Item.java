package com.retail.core;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

public class Item implements Serializable{
	
//Attributes
private long upc;
private String description;
private double price;
private float weight;
private String shipMethod;
private double shippingCost;
private final String[] shippingMethod= {"AIR","GROUND","RAIL"};

//Constructor
public Item(long upc,String description,double price,float weight, String shipMethod)
{
this.upc=upc;
this.description=description;
this.price=price;
this.weight=weight;
this.shipMethod=shipMethod;
}

//Getters and Setters
public double getShippingCost() {
	return shippingCost;
}

public void setShippingCost(double shippingCost) {
	this.shippingCost = shippingCost;
}

public long getUpc() {
	return upc;
}

public void setUpc(long upc) {
	this.upc = upc;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public double getPrice() {
	return price;
}

public void setPrice(double price) {
	this.price = price;
}

public float getWeight() {
	return weight;
}

public void setWeight(float weight) {
	this.weight = weight;
}

public void setShipMethod(String shipMethod) {
	this.shipMethod = shipMethod;
}

public String getShipMethod() {
	return shipMethod;
}

//Override the hashCode() and equals()

public int hashCode()
{
	return (int) this.getUpc();
}

public boolean equals(Object obj)
{
	Item item=(Item)obj;
	if(this.getUpc()==item.getUpc())
	{
		return true;
	}
	return false;
}


}
