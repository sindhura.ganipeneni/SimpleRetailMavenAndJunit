package com.retail.core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	
	private Item i1;

	@Before
	public void doSetUp()
	{
	i1=	new Item(567321101984l,"CD - Michael Jackson, Thriller",23.88,0.50f,"GROUND");
	}
	@Test
	public void testgetUpc() {
		assertEquals(567321101984l, i1.getUpc(),0);
	}

}
