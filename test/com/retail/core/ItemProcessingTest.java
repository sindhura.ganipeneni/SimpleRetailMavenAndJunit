package com.retail.core;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class ItemProcessingTest {

	private String input;
	private boolean result;
	@Parameters
	public static Collection<Object[]> data() {
	return Arrays.asList(new Object[][]{
		{"AIR",true},
		{"GROUND",true},
		{"RAIL",true},
		{"ANYTHINGELSE",false}
	});
	}
	
	 public ItemProcessingTest(String input,boolean result) {
		this.input=input;
		this.result=result;
	}
	@Test
	public void testisShippingMethodValid() {
		assertEquals(result,ItemProcessing.isValidShippingMethod(input));
	}

}
